import pandas as pd
import plotly.express as px
import statistics
import plotly.graph_objects as go


def clean_data():
    """this function will clean the data by getting rid of nulls, duplicate columns, and missing rows."""

    pf = pd.read_csv("netflix.csv")
    pf = pf.dropna()
    pf = pf.drop_duplicates()
    return pf


def count_elements_in_list(list1):
    """This function gets a list and return a dictionary with count of all the elements in it."""

    all_values = sorted(list(list1))
    unique_values = sorted(list(set(all_values)))
    count_dict = {}
    for ut in unique_values:
        count_dict.setdefault(ut, all_values.count(ut))

    return count_dict


def calculate_content_type():
    """This function will calculate what percentage of the movies are tv shows and what percentage are movies and finally
    generate a pie chart."""

    count_types_dict = count_elements_in_list(df["type"])
    values = list(count_types_dict.values())
    labels = list(count_types_dict.keys())
    fig = px.pie(values=values, names=labels, width=400, height=400, hover_name=labels,
                 title='Contents Type in Percentage',
                 color=labels)
    fig.layout.template = 'plotly_dark'
    fig.write_image("Contents Type in Percentage.png")
    # fig.show()


def count_content_by_country():
    """This function calculates all the content based on the country that is was generated in."""

    count_counties_dict = count_elements_in_list(df["country"])
    sorted_countries_list = sorted(count_counties_dict.items(), key=lambda x: x[1], reverse=True)
    sorted_countries_dict = dict(sorted_countries_list)
    avg_number_content = statistics.mean(list(sorted_countries_dict.values()))

    for c in list(sorted_countries_dict.keys()):
        if sorted_countries_dict[c] <= avg_number_content:
            sorted_countries_dict.pop(c)

    fig = px.bar(x=list(sorted_countries_dict.keys()), y=list(sorted_countries_dict.values()),
                 title="Number of Total Contents by Country").update_layout(xaxis_title="Country",
                                                                            yaxis_title="Number of Contents")
    fig.layout.template = 'plotly_dark'
    fig.write_image("Number of Total Contents by Country.png")
    # fig.show()


def calculate_number_of_types_by_country(df):
    """This function shows what portion of the content for each country is tv shows and movies in a clustered column."""
    df = df.dropna()
    countries = list(df["country"])
    unique_countries = list(set(countries))
    uc_dict = {}
    totals = []

    for uc in unique_countries:
        pf = df[df["country"] == uc]
        types = list(pf["type"])
        unique_types = list(set(types))
        dict_country = {}
        total = 0
        for ut in unique_types:
            total += types.count(ut)
            dict_country.setdefault(ut, types.count(ut))
        totals.append(total)
        dict_country.setdefault("Total", total)
        uc_dict.setdefault(uc, dict_country)

    avg_total = statistics.mean(totals)
    for uc in list(uc_dict.keys()):
        if uc_dict[uc]["Total"] <= avg_total:
            uc_dict.pop(uc)

    # sorted_uc_list = sorted(uc_dict.values(), key=lambda x: x[1], reverse=True)
    # uc_dict = dict(sorted_uc_list)

    keys = list(uc_dict.keys())
    keys.remove("Not Given")
    movies = []
    tvs = []
    for uc in keys:
        info = uc_dict[uc]
        if "Movie" in list(info.keys()):
            movies.append(info["Movie"])
        if "TV Show" in list(info.keys()):
            tvs.append(info["TV Show"])

    fig = go.Figure(go.Bar(x=keys, y=movies, name='Movies'))
    fig.add_trace(go.Bar(x=keys, y=tvs, name='TV Show'))

    fig.update_layout(xaxis_title="Country", barmode='stack', title="Type by Country")
    fig.update_xaxes(categoryorder='category ascending')
    fig.layout.template = 'plotly_dark'
    fig.write_image("Type_by_Country.png")
    # fig.show()


def calculate_number_of_new_content_annualLy():
    """This function calculates the number of new content annually and create a line chart."""

    years = list(df["release_year"])
    unique_years = list(set(years))
    unique_years.sort()
    years_dict = {}
    for uy in unique_years:
        years_dict.setdefault(uy, years.count(uy))
        # print(uy)

    duration = 40
    eligible_year = years[-1] - duration
    for y in list(years_dict.keys()):
        if y <= eligible_year:
            years_dict.pop(y)

    fig = px.line(x=list(years_dict.keys()), y=list(years_dict.values()), title="Number of New Content").update_layout(
        xaxis_title="Year")
    fig.layout.template = 'plotly_dark'
    fig.write_image("new_content_annually.png")
    # fig.show()


def calculate_type_by_director(df, multiple=3):
    """This function calculate the number of each type of content produced by each director and generate a stack column
    chart."""

    df = df.dropna()
    directors = list(df["director"])
    unique_directors = list(set(directors))
    ud_dict = {}
    totals = []

    for ud in unique_directors:
        pf = df[df["director"] == ud]
        types = list(pf["type"])
        unique_types = list(set(types))
        dict_country = {}
        total = 0
        for ut in unique_types:
            total += types.count(ut)
            dict_country.setdefault(ut, types.count(ut))
        totals.append(total)
        dict_country.setdefault("Total", total)
        ud_dict.setdefault(ud, dict_country)

    avg_total = statistics.mean(totals)
    for ud in list(ud_dict.keys()):
        if ud_dict[ud]["Total"] <= avg_total * 3:
            ud_dict.pop(ud)

    # sorted_uc_list = sorted(uc_dict.values(), key=lambda x: x[1], reverse=True)
    # uc_dict = dict(sorted_uc_list)

    keys = list(ud_dict.keys())
    keys.remove("Not Given")
    movies = []
    tvs = []
    for ud in keys:
        info = ud_dict[ud]
        if "Movie" in list(info.keys()):
            movies.append(info["Movie"])
        if "TV Show" in list(info.keys()):
            tvs.append(info["TV Show"])

    fig = go.Figure(go.Bar(x=keys, y=movies, name='Movies'))
    fig.add_trace(go.Bar(x=keys, y=tvs, name='TV Show'))

    fig.update_layout(xaxis_title="Director", barmode='stack', title="Type by Director")
    fig.update_xaxes(categoryorder='category ascending')
    fig.layout.template = 'plotly_dark'
    fig.write_image("Type_by_Director.png")
    # fig.show()


def count_by_genre():
    """This function calculates each type of genre and create a column chart."""

    genres_dict = count_elements_in_list(df["listed_in"])
    avg_total = statistics.mean(list(genres_dict.values()))
    for ug in list(genres_dict.keys()):
        if genres_dict[ug] <= avg_total * 3:
            # print(genres_dict[ug])
            genres_dict.pop(ug)

    # sorted_ug_list = sorted(genres_dict.values(), key=lambda x: x[1], reverse=True)
    # ug_dict = dict(sorted_ug_list)
    ug_dict = genres_dict

    fig = px.bar(x=list(ug_dict.keys()), y=list(ug_dict.values()),
                 title="Number of Contents by Genres").update_layout(xaxis_title="Genre",
                                                                     yaxis_title="Number of Contents")
    fig.layout.template = 'plotly_dark'
    fig.write_image("Number of Total Contents by Genre.png")
    # fig.show()


def find_oldest(n=10):
    """This function will find out the *n* oldest content on Netflix."""

    titles = list(df["title"])
    release_years = list(df["release_year"])
    titles_dict = {}
    i = -1
    for t in titles:
        i += 1
        titles_dict.setdefault(t, release_years[i])

    sorted_tt_list = sorted(titles_dict.items(), key=lambda x: x[1])
    tt_dict = dict(sorted_tt_list)
    i = 0
    for k, v in tt_dict.items():
        i += 1
        if i <= n:
            print(k, v)


def count_new_content_by_type_annaully():
    """This function will calculate the number of new content type annually and generate a line chart at the end."""

    years = list(set(df["release_year"]))
    unique_content = list(set(df["type"]))
    years_dict = {}
    movies = []
    shows = []
    for y in years:
        ff = df[df["release_year"] == y]
        contents = list(ff["type"])
        dict_year = {}
        for uc in unique_content:
            count = contents.count(uc)
            dict_year.setdefault(uc, count)
            if uc == "Movie":
                movies.append(count)
            elif uc == "TV Show":
                shows.append(count)
        years_dict.setdefault(y, dict_year)

    fig = go.Figure()
    fig.add_trace(go.Scatter(x=years, y=movies, name="Movies"))
    fig.add_trace(go.Scatter(x=years, y=shows, name="TV Shows"))
    fig.update_layout(xaxis_title="Years", yaxis_title="Count", barmode='stack',
                      title="Number of new content by type annually")
    fig.update_xaxes(categoryorder='category ascending')
    fig.layout.template = 'plotly_dark'
    fig.write_image("Number_of_new_content_by_type_annually.png")
    fig.show()


def calculate_uploads_netflix():
    """This function calculate the number of new content added on Netflix."""

    added_date = list(df["date_added"])
    added_years = []
    for date in added_date:
        date = date.split("/")
        year = date[2]
        added_years.append(year)

    added_years = sorted(added_years)
    years_dict = count_elements_in_list(added_years)

    fig = go.Figure()
    fig.add_trace(go.Scatter(x=list(years_dict.keys()), y=list(years_dict.values())))
    fig.update_layout(xaxis_title="Years", yaxis_title="Count", barmode='stack',
                      title="Number of new release on netflix annually")
    fig.update_xaxes(categoryorder='category ascending')
    fig.layout.template = 'plotly_dark'
    fig.write_image("Number_of_new_release_on_netflix_annually.png")
    fig.show()


df = clean_data()
# calculate_content_type()
# count_content_by_country()
# calculate_content_type(df)
# calculate_number_of_new_content_annually()
# calculate_type_by_director(df, 3)
# count_by_genre()
# find_oldest(15)
# count_new_content_by_type_annaully()
calculate_uploads_netflix()
